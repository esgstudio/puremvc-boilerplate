import Boilerplate from './game/app/PureMVCBoilerplate';

window.onload = function() {

    let app: Boilerplate = new Boilerplate();
    app.boot();
    
    let container: HTMLDivElement = <HTMLDivElement>(document.getElementById('test-app'));
    container.appendChild(app.getView());

};