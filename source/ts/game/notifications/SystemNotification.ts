

export class SystemNotification {
    public static INITIALISE: string = 'SystemNotification:INITIALISE';
    public static STARTUP_COMPLETE: string = 'SystemNotification:STARTUP_COMPLETE';
}