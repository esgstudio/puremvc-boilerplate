

export class GameNotification {
    public static GENERATE_NEW_SWATCH_COLORS: string = 'GameNotification:GENERATE_NEW_SWATCH_COLORS';
    public static SWATCH_COLORS_UPDATED: string = 'GameNotification:SWATCH_COLORS_UPDATED';
    public static NUM_SWATCHES_UPDATED: string = 'GameNotification:NUM_SWATCHES_UPDATED';
}