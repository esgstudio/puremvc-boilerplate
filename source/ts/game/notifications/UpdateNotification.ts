/**
 * Update Notifications
 * @enum
 */
export default class UpdateNotification {

	public static ADD:string = 'UpdateNotification.ADD';
	public static REMOVE:string = 'UpdateNotification.REMOVE';
	public static START:string = 'UpdateNotification.START';
	public static STOP:string = 'UpdateNotification.STOP';

	public static PAUSE:string = 'UpdateNotification.PAUSE';
	public static RESUME:string = 'UpdateNotification.RESUME';

}