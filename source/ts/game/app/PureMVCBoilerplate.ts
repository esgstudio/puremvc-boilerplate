import {App} from './App';
import {InitialiseGame} from '../commands/startup/InitialiseGame';
import {SystemNotification} from '../notifications/SystemNotification';

export default class PureMVCBoilerplate extends App {

	public initializeController():void {
		super.initializeController();
		this.registerCommand(SystemNotification.INITIALISE, InitialiseGame);
	}
}
