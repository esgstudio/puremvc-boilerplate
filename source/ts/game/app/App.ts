const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');
import {SystemNotification} from '../notifications/SystemNotification';
import {PixiMediator} from '../view/mediator/PixiMediator';

export class App extends mvc.Facade {

	/**
	 * Initialises the game into the boot state.
	 */
	public boot(): void {
		this.sendNotification(SystemNotification.INITIALISE);
	}


	/**
	 * Returns the renderer's view element.
	 * @returns {HTMLCanvasElement}
	 */
	public getView(): HTMLCanvasElement {
		let m = <PixiMediator> (this.retrieveMediator(PixiMediator.NAME));
		return m.getView();
	}
}
