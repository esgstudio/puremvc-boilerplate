import {SwatchProxy} from '../model/SwatchProxy';

const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');

export class GenerateNewSwatchColorsCmd extends mvc.SimpleCommand {

    public execute():void {

        const swatchProxy:SwatchProxy = this.facade.retrieveProxy(SwatchProxy.NAME) as SwatchProxy;

        const newCols:number[] = [];

        for (let i:number = 0; i < swatchProxy.numSwatches; i++) {
            newCols.push(Math.random() * 0xFFFFFF);
        }

        swatchProxy.swatchColors = newCols;
    }

}