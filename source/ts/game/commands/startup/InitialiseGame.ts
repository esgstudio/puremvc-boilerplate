const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');
import {AddRootContainersCmd} from './AddRootContainers';
import {MakeMediators} from './MakeMediators';
import {MakeDisplayContainers} from './MakeDisplayContainers';
import {RegisterCommands} from './RegisterCommands';
import {MakeModels} from './MakeModels';
import {StartUpCompleteCmd} from './StartUpCompleteCmd';

export class InitialiseGame extends mvc.MacroCommand {

    public initializeMacroCommand():void {
        super.initializeMacroCommand();

        this.addSubCommand(RegisterCommands);
        this.addSubCommand(MakeModels);
        this.addSubCommand(MakeDisplayContainers);
        this.addSubCommand(MakeMediators);
        this.addSubCommand(AddRootContainersCmd);
        this.addSubCommand(StartUpCompleteCmd);
    }
}