import {PixiMediator} from '../../view/mediator/PixiMediator';
const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');
import {DisplayContainerNames} from '../../constants/DisplayContainerNames';

export class AddRootContainersCmd extends mvc.SimpleCommand {

	public execute():void {
		let layerOrder:string[] = [
			DisplayContainerNames.BACKGROUND_CONTAINER,
			DisplayContainerNames.GAME_CONTAINER,
			DisplayContainerNames.FOREGROUND_CONTAINER
		];


		let pixi:PixiMediator = this.facade.retrieveMediator(PixiMediator.NAME) as PixiMediator;
		pixi.addContainersToStage(layerOrder);
	}
}
