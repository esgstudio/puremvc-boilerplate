import {SystemNotification} from '../../notifications/SystemNotification';
import {GameNotification} from '../../notifications/GameNotification';

const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');

export class StartUpCompleteCmd extends mvc.SimpleCommand {

    public execute():void {
        this.sendNotification(GameNotification.GENERATE_NEW_SWATCH_COLORS);
        this.sendNotification(SystemNotification.STARTUP_COMPLETE);
    }
}