const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');
import {DisplayContainerNames} from '../../constants/DisplayContainerNames';
import Container = PIXI.Container;
import {ContainerProxy} from '../../model/ContainerProxy';

export class MakeDisplayContainers extends mvc.SimpleCommand {

	public execute(notification:puremvc.INotification):void {

		let gameContainerDictionary:{[key:string]:Container} = {};

		gameContainerDictionary[DisplayContainerNames.GAME_CONTAINER] = new Container();
		gameContainerDictionary[DisplayContainerNames.GAME_CONTAINER].name = DisplayContainerNames.GAME_CONTAINER;

		// all these get added to the GAME_CONTAINER
		gameContainerDictionary[DisplayContainerNames.BACKGROUND_CONTAINER] = new Container();
		gameContainerDictionary[DisplayContainerNames.BACKGROUND_CONTAINER].name = DisplayContainerNames.BACKGROUND_CONTAINER;

		gameContainerDictionary[DisplayContainerNames.FOREGROUND_CONTAINER] = new Container();
		gameContainerDictionary[DisplayContainerNames.FOREGROUND_CONTAINER].name = DisplayContainerNames.FOREGROUND_CONTAINER;

		this.facade.registerProxy(new ContainerProxy(gameContainerDictionary));
	}
}
