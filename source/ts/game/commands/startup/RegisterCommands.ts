import {GameNotification} from '../../notifications/GameNotification';
import {GenerateNewSwatchColorsCmd} from '../GenerateNewSwatchColorsCmd';

const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');

export class RegisterCommands extends mvc.SimpleCommand {

    public execute(notification: puremvc.INotification): void {

        this.facade.registerCommand(GameNotification.GENERATE_NEW_SWATCH_COLORS, GenerateNewSwatchColorsCmd);

    }
}
