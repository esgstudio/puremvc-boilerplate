import UpdateLoopMediator from '../../view/mediator/UpdateLoopMediator';
const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');
import {PixiMediator} from '../../view/mediator/PixiMediator';
import {HEIGHT, WIDTH} from '../../constants/GameDetail';
import {GameScreenMediator} from '../../view/mediator/GameScreenMediator';
import {BackgroundMediator} from '../../view/mediator/BackgroundMediator';


export class MakeMediators extends mvc.SimpleCommand {

    /**
     * Execute
     * @param {Notification} notification
     */
    public execute(notification: puremvc.INotification): void {
        let pixiMediator:PixiMediator = new PixiMediator(WIDTH, HEIGHT);
        this.facade.registerMediator(pixiMediator);

        let updateLoopMediator = new UpdateLoopMediator(false);
        updateLoopMediator.add(pixiMediator);
        updateLoopMediator.start();
        this.facade.registerMediator(updateLoopMediator);

        let bgMediator:BackgroundMediator = new BackgroundMediator();
        this.facade.registerMediator(bgMediator);

        let gameScreenMediator:GameScreenMediator = new GameScreenMediator();
        this.facade.registerMediator(gameScreenMediator);
    }

}
