const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');

import {SwatchProxy} from '../../model/SwatchProxy';
import {DeviceProxy} from '../../model/DeviceProxy';

export class MakeModels extends mvc.SimpleCommand {

	public execute(notification:puremvc.INotification):void {

		let deviceProxy:DeviceProxy = new DeviceProxy(DeviceProxy.NAME);
		this.facade.registerProxy(deviceProxy);

		this.facade.registerProxy(new SwatchProxy());
	}
}
