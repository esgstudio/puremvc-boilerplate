import Container = PIXI.Container;
import Graphics = PIXI.Graphics;

export class BackgroundView extends Container{

    private _width:number = 0;
    private _height:number = 0;

    private _bg:Graphics;

    constructor() {
        super();

        this._bg = new Graphics();
        this.addChild(this._bg);
    }

    public setSize(w:number, h:number):void {
        this._width = w;
        this._height = h;
    }

    public fillBackground(col:number):void {
        this._bg.clear();

        this._bg.beginFill(col, 1);
        this._bg.drawRect(0, 0, this._width, this._height);
    }
}