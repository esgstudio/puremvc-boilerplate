import Container = PIXI.Container;
import {IUpdatable} from '../../interfaces/IUpdatable';
import {Signal} from 'signals';
import Text = PIXI.Text;
import TextStyleOptions = PIXI.TextStyleOptions;
import {ColorSwatchButton} from './buttons/ColorSwatchButton';
import {HEIGHT, WIDTH} from '../../constants/GameDetail';

export class GameView extends Container implements IUpdatable {

    private _title:Text;
    private _swatchContainer:Container;
    private _swatches:ColorSwatchButton[];

    public changeBackground: TypedSignal<number> = new Signal();
    public requestNewSwatches: Signal = new Signal();

    constructor(){
        super();
    }

    public buildView(numSwatches:number, colors:number[]) {
        this.createText();
        this.createSwatches(numSwatches, colors);
    }

    public updateSwatchColours(cols:number[]):void {
        if (!this._swatches) return;

        this.validateData(this._swatches.length, cols);

        for (let i:number = 0; i < cols.length; i++) {
            this._swatches[i].assignColour(cols[i]);
        }
    }

    private createText():void {
        let style:TextStyleOptions = {
            fontSize: 100,
            fill: 'black',
            stroke: 'white',
            strokeThickness: 10,
            lineJoin: 'round',
            miterLimit: 9
        };

        this._title = new Text('PureMVC Demo', style);
        this._title.x = (WIDTH * 0.5) - (this._title.width * 0.5);
        this._title.y = (HEIGHT * 0.5) - (this._title.height * 0.5);
        this.addChild(this._title);
    }

    private createSwatches(numSwatches:number, colors:number[]):void {

        this.validateData(numSwatches, colors);

        this._swatchContainer = new Container();
        this.addChild(this._swatchContainer);

        const swatchWidth:number = 150;
        const swatchHeight:number = 80;
        const swatchSpacer:number = 30;

        this._swatches = [];
        let swatch:ColorSwatchButton;
        let xPos:number = swatchWidth * 0.5;

        for (let i:number = 0; i < numSwatches; i++) {
            swatch = new ColorSwatchButton(swatchWidth, swatchHeight, colors[i]);
            swatch.swatchSelected.add(this.onSwatchSelected, this);
            this._swatchContainer.addChild(swatch);
            swatch.x = xPos;
            xPos += swatchWidth + swatchSpacer;
            this._swatches.push(swatch);
        }

        this._swatchContainer.x = (WIDTH * 0.5) - (this._swatchContainer.width * 0.5);
        this._swatchContainer.y = HEIGHT - this._swatchContainer.height - 80;
    }

    private onSwatchSelected(col:number):void {
        this.changeBackground.dispatch(col);

        if (this.needsNewSwatches()) {
            this.requestNewSwatches.dispatch();
        }
    }

    private validateData(numSwatches:number, colors:number[]):void {
        if (numSwatches != colors.length) {
            throw Error("*** NUMBER OF SWATCHES DOES NOT MATCH SUPPLIED COLORS ***")
        }
    }

    private needsNewSwatches():boolean {
        let retVal:boolean = true;

        for (let i:number = 0; i < this._swatches.length; i++) {
            if(!this._swatches[i].viewed) {
                retVal = false;
            }
        }

        return retVal;
    }

    public update(delta:number) {
        // dont need an update loop inn this demo yet
    }

    public destroy() {
        this.changeBackground.dispose();
        this.changeBackground = null;
        super.destroy();
    }
}