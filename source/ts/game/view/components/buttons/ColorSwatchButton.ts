import {HitAreaButton} from './HitAreaButton';
import {Signal} from 'signals';

export class ColorSwatchButton extends HitAreaButton {

    private _btnWidth:number;
    private _btnHeight:number;
    private _color:number;

    private _viewed:boolean = false;

    public swatchSelected:TypedSignal<number>;

    public get viewed():boolean {
        return this._viewed;
    }

    constructor(width: number, height: number, col:number) {
        super(width, height);

        this.swatchSelected = new Signal();
        this._btnWidth = width;
        this._btnHeight = height;

        this.assignColour(col);
    }

    public assignColour(col:number):void {
        this._viewed = false;
        this._color = col;
        this._gfx.clear();
        this._gfx.beginFill(col, 1);
        this._gfx.lineStyle(4, 0,1);
        this._gfx.drawRect(-this._btnWidth * .5, -this._btnHeight * .5, this._btnWidth, this._btnHeight);
        this._gfx.endFill();
    }

    /**
     * Interaction Up Handler
     * @protected
     */
    protected interactionUpHandler(event?:any): void {
       super.interactionUpHandler(event);
       this._viewed = true;
       this.swatchSelected.dispatch(this._color);
    }

}