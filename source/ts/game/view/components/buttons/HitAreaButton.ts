import Rectangle = PIXI.Rectangle;
import { AbstractButton } from './AbstractButton';
import Graphics = PIXI.Graphics;

export class HitAreaButton extends AbstractButton {

	protected _gfx: Graphics;

	constructor(width: number, height: number) {
		super();

		this.init();
		this.hitArea = new Rectangle(-width * .5, -height * .5, width, height);
		this.interactive = true;
		this.buttonMode = true;

		this._gfx = new Graphics();
		this._gfx.beginFill(0, 0);
		this._gfx.drawRect(-width * .5, -height * .5, width, height);
		this._gfx.endFill();

		this.addChild(this._gfx);
	}
}
