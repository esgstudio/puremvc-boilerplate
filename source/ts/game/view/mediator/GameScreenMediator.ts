import {GameView} from '../components/GameView';
let mvc:typeof puremvc = require('puremvc-typescript-standard-framework');
import * as PIXI from 'pixi.js';
import Container = PIXI.Container;
import {DisplayContainerNames} from '../../constants/DisplayContainerNames';
import {ContainerProxy} from '../../model/ContainerProxy';
import UpdateNotification from '../../notifications/UpdateNotification';
import {SystemNotification} from '../../notifications/SystemNotification';
import {BackgroundNotification} from '../../notifications/BackgroundNotification';
import {SwatchProxy} from '../../model/SwatchProxy';
import {GameNotification} from '../../notifications/GameNotification';

export class GameScreenMediator extends mvc.Mediator<GameView> {

	public static NAME: string = 'GameScreenMediator';

	private _containerProxy:ContainerProxy;
	private _swatchProxy:SwatchProxy;

	constructor() {
		super(GameScreenMediator.NAME, new GameView());
		this.viewComponent.name = 'GameScreenMediator';
		this.retrieveProxies();
	}

	private retrieveProxies():void {
		this._containerProxy = this.facade.retrieveProxy(ContainerProxy.NAME) as ContainerProxy;
		this._swatchProxy = this.facade.retrieveProxy(SwatchProxy.NAME) as SwatchProxy;
	}

	public listNotificationInterests():string[] {
		return [
            SystemNotification.STARTUP_COMPLETE,
            GameNotification.SWATCH_COLORS_UPDATED
		];
	}

	public handleNotification(notification: puremvc.INotification):void {
		switch (notification.getName()) {

			case SystemNotification.STARTUP_COMPLETE:
                this.init();
				break;

			case GameNotification.SWATCH_COLORS_UPDATED:
                this.updateSwatchColors(notification.getBody());
				break;
		}
	}

    private init():void {
        let gameContainer: Container = this._containerProxy.getContainer(DisplayContainerNames.GAME_CONTAINER);
        gameContainer.addChild(this.viewComponent);

        this.viewComponent.buildView(this._swatchProxy.numSwatches, this._swatchProxy.swatchColors);
        this.viewComponent.changeBackground.add(this.onChangeBackground, this);
        this.viewComponent.requestNewSwatches.add(()=>{
            this.sendNotification(GameNotification.GENERATE_NEW_SWATCH_COLORS);
		});

        this.sendNotification(UpdateNotification.ADD, this.viewComponent);
    }

    private onChangeBackground(col:number):void {
		this.sendNotification(BackgroundNotification.CHANGE_BACKGROUND, col);
	}

    private updateSwatchColors(cols:number[]):void {
		this.viewComponent.updateSwatchColours(cols);
	}
}
