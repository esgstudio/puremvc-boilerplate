const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');
import {IUpdatable} from '../../interfaces/IUpdatable';
import * as PIXI from 'pixi.js';
import Container = PIXI.Container;
import SystemRenderer = PIXI.SystemRenderer;
import CanvasRenderer = PIXI.CanvasRenderer;
import WebGLRenderer = PIXI.WebGLRenderer;
import {ContainerProxy} from '../../model/ContainerProxy';
import {DeviceProxy} from '../../model/DeviceProxy';
import {BrowserName} from '../../constants/device/BrowserName';
import {DeviceVO} from '../../model/vo/DeviceVO';

import { RendererOptions } from 'pixi.js';

export class PixiMediator extends mvc.Mediator<Container> implements IUpdatable {

	public static NAME: string = 'PixiMediator';

    private _containerProxy: ContainerProxy;
    private _deviceProxy: DeviceProxy;

    protected _renderer: SystemRenderer = null;

	public getView(): HTMLCanvasElement {
		return this._renderer.view;
	}

	public get renderer(): WebGLRenderer | CanvasRenderer {
		return <WebGLRenderer | CanvasRenderer> this._renderer;
	}

	constructor(width: number, height: number) {
		super(PixiMediator.NAME, new Container());
		this.viewComponent.name = 'PIXI GAME';
		this.retrieveProxies();
		this.init(width, height);
	}

	private retrieveProxies():void {
        this._containerProxy = this.facade.retrieveProxy(ContainerProxy.NAME) as ContainerProxy;
        this._deviceProxy = this.facade.retrieveProxy(DeviceProxy.NAME) as DeviceProxy;
	}

	private init(width:number, height:number):void {
		this._renderer = this.createRenderer(width, height);
	}

	/**
	 * Will add the containers to the stage in the order they are found in the supplied array
	 * @param {string[]} rootContainers
	 */
	public addContainersToStage( rootContainers:string[]):void {

		let cntr:Container;

		do {
			// the containers should be build in a game/commands/MakeDisplayContaners
			cntr = this._containerProxy.getContainer(rootContainers.shift());
			if(cntr) {
				this.viewComponent.addChild(cntr);
			}

		} while (rootContainers.length > 0);
	}

	public listNotificationInterests() {
		return [];
	}

	public handleNotification(notification: puremvc.INotification): void {

	}

	/**
	 * Creates configures and returns
	 * @returns {SystemRenderer}
	 */
	protected createRenderer(width: number, height: number): SystemRenderer {
		let rendererOptions: RendererOptions = {
			antialias: false,
			transparent: false,
			resolution: window.devicePixelRatio,
			backgroundColor: 0xFFFFFF
		};

		let deviceVO:DeviceVO = this._deviceProxy.getData();

		let renderer: SystemRenderer;

		if (deviceVO.browserName == BrowserName.INTERNET_EXPLORER && Math.floor(deviceVO.browserVersion)==11){
			//use canvas on IE 11
			renderer = new PIXI.CanvasRenderer(width, height, rendererOptions);
		} else {
			renderer = PIXI.autoDetectRenderer(width, height, rendererOptions);
		}

		if (renderer.type == PIXI.RENDERER_TYPE.CANVAS) {
			renderer.resolution = 1;
		}

		return renderer;
	}

	/**
	 * Update
	 * @param {Number} delta
	 */
	public update(delta: number): void {
		this._renderer.render(this.viewComponent);
	}
}
