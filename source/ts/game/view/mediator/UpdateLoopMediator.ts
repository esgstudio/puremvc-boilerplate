import UpdateNotification from '../../notifications/UpdateNotification';
import {DateUtils} from '../../utils/DateUtils';
import {IUpdatable} from '../../interfaces/IUpdatable';
var mvc:typeof puremvc = require('puremvc-typescript-standard-framework');
var Stats = require('stats.js');


/**
 * Update Loop
 * also referred to as the Game Loop when used within games
 */
export default class UpdateLoopMediator extends mvc.Mediator<Stats> {

	// Contains ---------------------------------------------------------------
	public static NAME:string = 'UpdateLoopMediator';

    // Variables --------------------------------------------------------------
    /**
     * An array of updates
     * @type {Array.<IUpdatable>}
     * @private
     */
    private _updates:IUpdatable[] = [];

    /**
     * Last time updated in milliseconds since the epoch
     * @type {number}
     * @private
     */
    private _lastTimeUpdated:number = 0;

    /**
     * Is Updating
     * @type {boolean}
     */
    private _isUpdating:boolean = false;

	/**
	 * Is Paused
	 * @type {boolean}
	 * @private
	 */
	private _isPaused:boolean = false;

    /**
     * Updates pending removal
     * @type {Array.<IUpdatable>}
     * @private
     */
    private _updatesPendingRemoval:IUpdatable[] = [];

    private _now:Function;

    // Getters and Setters ----------------------------------------------------
    public isUpdating():boolean { return this._isUpdating; }
	
    /**
     * Update Loop
     * @constructor
     */
    constructor (statsEnabled:boolean = true) {
       super(UpdateLoopMediator.NAME, statsEnabled ? new Stats() : null);
        if (statsEnabled) {
            this.viewComponent.dom.classList.add('stats');
        }
        this._now = DateUtils.now();
        window.requestAnimationFrame(this._loop.bind(this));
    }

	/**
	 * List Notification Interests
	 * A list of notification names that this Mediator is interested in
	 * @return {Array} - The list of Notification names.
	 */
	public listNotificationInterests():Array<string> {
		return [	UpdateNotification.PAUSE,
					UpdateNotification.RESUME,
					UpdateNotification.ADD,
					UpdateNotification.REMOVE,
					UpdateNotification.START,
					UpdateNotification.STOP 	];
	}

	/**
	 * Handle Notifications.
	 * @param {Notification} notification
	 */
	public handleNotification( notification:puremvc.INotification ):void {
		switch (notification.getName())
		{
			case UpdateNotification.ADD:
				this.add(<IUpdatable>notification.getBody());
				break;
			case UpdateNotification.REMOVE:
				this.remove(<IUpdatable>notification.getBody());
				break;

			case UpdateNotification.RESUME:
				this._isPaused = false;
				// intentionally no break point
			case UpdateNotification.START:
				this.start();
				break;

			case UpdateNotification.PAUSE:
				this._isPaused = true;
				// intentionally no break point
			case UpdateNotification.STOP:
				this.stop();
				break;
		}
	}

    /**
     * Add
     * @param {IUpdatable} update - object that contains a update function
     */
    public add(update:IUpdatable): void {
        this._updates.push(update);
    }

    /**
     * Remove
     * Updates will be removed at the start of the next loop
     * @param {IUpdatable} update - object that contains a update function
     */
    public remove(update:IUpdatable): void {

        // Error Checking - is the Update in the loop?
        var isInUpdateLoop = false;

        var i:number;
        for (i = 0; i < this._updates.length; i++) {
            if (this._updates[i] === update) {
                isInUpdateLoop = true;
                break;
            }
        }

		if (!isInUpdateLoop) {
			console.error('ERROR - Update [' + update + '] not found ' + this);
		}

        // Error Checking - is the Update already pending removal?
        for (i = 0; i < this._updatesPendingRemoval.length; i++) {
            if (this._updatesPendingRemoval[i] === update) {
                console.error('WARNING - Update [' + update + '] is already marked for removal ' + this);
                return;
            }
        }

        // Add to the list of Updates to be removed in the next loop
        if (isInUpdateLoop) {
            this._updatesPendingRemoval.push(update);
        }
    }

	/**
	 * Remove
	 * Removes immediately
	 * @param {IUpdatable} update
	 * @private
	 */
	private _remove(update:IUpdatable) {
		for (var i = 0; i < this._updates.length; i++) {
			if (this._updates[i] === update) {
				this._updates.splice(i, 1);
				return;
			}
		}
		console.warn('ERROR - iUpdate [' + update + '] not found in the update array ' + this);
	}

    /**
     * Start Update Loop
     */
    public start(): void {
		if (this._isPaused) {
			console.warn('cannot start - is paused! '+this);
		} else if (!this._isUpdating) {
            this._isUpdating = true;
            this._lastTimeUpdated = this._now();
        } else {
            console.warn('cannot start - is already updating! '+this);
        }
    }


    /**
     * Stop Update Loop
     */
    public stop(): void {
        this._isUpdating = false;
    }


    /**
     * Update Loop
     * @private
     */
    private _loop(): void {
		if (this.viewComponent) this.viewComponent.begin();

        if (this._isUpdating) {
            // Check if there are any IUpdatables to remove
            if (this._updatesPendingRemoval.length > 0)
            {
                for (let i=0; i<this._updatesPendingRemoval.length; i++)
                {
                    this._remove(this._updatesPendingRemoval[i]);
                }
                this._updatesPendingRemoval = [];
            }

            var deltaTime: number = this._now() - this._lastTimeUpdated;
            if(deltaTime>=8 ) {
                var updates: IUpdatable[] = this._updates;
                let i = this._updates.length;
                while (i--) {
                    updates[i].update(deltaTime);
                }

                this._lastTimeUpdated = this._now();
            }
        }

        // Continue Looping
        window.requestAnimationFrame(this._loop.bind(this));
		if (this.viewComponent) this.viewComponent.end();
    }



	private _info(title:string,data:any):void
	{
		console.info('%c '+title,'color:blue',data);
	}
}
