import {BackgroundView} from '../components/BackgroundView';
import {SystemNotification} from '../../notifications/SystemNotification';
import {HEIGHT, WIDTH} from '../../constants/GameDetail';
import {ContainerProxy} from '../../model/ContainerProxy';
import {DisplayContainerNames} from '../../constants/DisplayContainerNames';
import Container = PIXI.Container;
import {BackgroundNotification} from '../../notifications/BackgroundNotification';
const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');

export class BackgroundMediator extends mvc.Mediator<BackgroundView> {

    public static NAME: string = 'BackgroundMediator';

    private _containerProxy:ContainerProxy;

    public viewComponent:BackgroundView;

    constructor() {
        super(BackgroundMediator.NAME, new BackgroundView());
        this.viewComponent.name = 'Background';
        this.retrieveProxies();
    }

    private retrieveProxies():void {
        this._containerProxy = this.facade.retrieveProxy(ContainerProxy.NAME) as ContainerProxy;
    }

    public listNotificationInterests() {
        return [
            SystemNotification.STARTUP_COMPLETE,
            BackgroundNotification.CHANGE_BACKGROUND
        ];
    }

    public handleNotification(notification: puremvc.INotification): void {
        switch (notification.getName()) {

            case SystemNotification.STARTUP_COMPLETE:
                let gameContainer: Container = this._containerProxy.getContainer(DisplayContainerNames.GAME_CONTAINER);
                gameContainer.addChild(this.viewComponent);
                this.viewComponent.setSize(WIDTH, HEIGHT);
                this.viewComponent.fillBackground(0xffffff);
                break;

            case BackgroundNotification.CHANGE_BACKGROUND:
                this.viewComponent.fillBackground(notification.getBody());
                break;
        }
    }

}