import { EDeviceType } from '../../constants/device/EDeviceType';

/**
 * Device
 * Value Object
 */
export class DeviceVO {

	// Values -----------------------------------------------------------------
	private _deviceType: EDeviceType;
	private _browserName: string;
	private _browserVersion: number;
	private _osVersion: string;
	private _osName: string;
	private _aspectRatio: number;

	/**
	 * @constructor
	 */
	constructor() {
	}

	get deviceType(): EDeviceType {
		return this._deviceType;
	}

	set deviceType(value: EDeviceType) {
		this._deviceType = value;
	}

	get browserName(): string {
		return this._browserName;
	}

	set browserName(value: string) {
		this._browserName = value;
	}

	get browserVersion(): number {
		return this._browserVersion;
	}

	set browserVersion(value: number) {
		this._browserVersion = value;
	}

	get osVersion(): string {
		return this._osVersion;
	}

	set osVersion(value: string) {
		this._osVersion = value;
	}

	get osName(): string {
		return this._osName;
	}

	set osName(value: string) {
		this._osName = value;
	}

	get aspectRatio(): number {
		return this._aspectRatio;
	}

	set aspectRatio(value: number) {
		this._aspectRatio = value;
	}
}
