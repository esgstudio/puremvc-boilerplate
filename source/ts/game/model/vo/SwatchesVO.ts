
export class SwatchesVO {

    constructor(
        public colors:number[] = [],
        public numSwatches: number = 3
    ) { }

}