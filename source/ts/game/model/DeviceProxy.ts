const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');

import { EDeviceType } from '../constants/device/EDeviceType';
import { DeviceVO } from './vo/DeviceVO';
import { DeviceOS } from '../constants/device/DeviceOS';
import { Devices } from '../constants/device/Devices';
import { BrowserName } from '../constants/device/BrowserName';

export class DeviceProxy extends mvc.Proxy<DeviceVO> {

	public static NAME: string = 'DeviceProxy';

	constructor(proxyName: string) {
		super(proxyName, new DeviceVO());
		this._detect();
	}
    /**
     * Detect OS, Browser and Device Type
     */
	private _detect(): void {
		// Device OS
		this._detectOS();
		// Browser Name
		this._detectBrowser();
		// Aspect Ratio
		this._detectAspectRatio();
		// Device Type
		this._detectDeviceType();

		console.warn(this.data);
	}

    /**
     * Util for finding Matches
     */
	private _findMatch(rules, userAgent): any {
		for (let key in rules) {
			if (Object.prototype.hasOwnProperty.call(rules, key)) {
				if (rules[key].test(userAgent)) {
					return key;
				}
			}
		}
		return null;
	};

    /**
     * Convert Properties To Regular Expressions
     * @param object
     * @returns {any}
     * @private
     */
	private _convertPropsToRegExp(object: any): any {
		for (let key in object) {
			if (Object.prototype.hasOwnProperty.call(object, key)) {
				object[key] = new RegExp(object[key], 'i');
			}
		}
		return object;
	}

    /**
     * Detect Device Type
     * @private
     */
	private _detectDeviceType(): void {
		let nAgent: string = navigator.userAgent;

		let mobiles: any = this._convertPropsToRegExp(Devices.MOBILES);
		let tablets: any = this._convertPropsToRegExp(Devices.TABLETS);

		let isMobile: boolean = this._findMatch(mobiles, nAgent) != null;
		let isTablet: boolean = this._findMatch(tablets, nAgent) != null;


		let deviceType: EDeviceType = isMobile ? EDeviceType.MOBILE : isTablet ? EDeviceType.TABLET : EDeviceType.DESKTOP;

		if (deviceType == EDeviceType.DESKTOP) {
			switch (this.data.osName) {
				case DeviceOS.ANDROID:
				case DeviceOS.IOS:
				case DeviceOS.WINDOWS_MOBILE:
					// This isn't a desktop
					deviceType = EDeviceType.MOBILE;
					break;
			}
		}

		this.data.deviceType = deviceType;
	}

    /**
     * Detect OS
     * @private
     */
	private _detectOS(): void {
		let nVersion: string = navigator.appVersion;
		let nAgent: string = navigator.userAgent;
		let osName: string = '';
		let osVersion: any;
		let clientStrings = [
			{ s: 'Windows 10', r: /(Windows 10.0|Windows NT 10.0)/ },
			{ s: 'Windows 8.1', r: /(Windows 8.1|Windows NT 6.3)/ },
			{ s: 'Windows 8', r: /(Windows 8|Windows NT 6.2)/ },
			{ s: 'Windows 7', r: /(Windows 7|Windows NT 6.1)/ },
			{ s: 'Windows Vista', r: /Windows NT 6.0/ },
			{ s: 'Windows Server 2003', r: /Windows NT 5.2/ },
			{ s: 'Windows XP', r: /(Windows NT 5.1|Windows XP)/ },
			{ s: 'Windows 2000', r: /(Windows NT 5.0|Windows 2000)/ },
			{ s: 'Windows ME', r: /(Win 9x 4.90|Windows ME)/ },
			{ s: 'Windows 98', r: /(Windows 98|Win98)/ },
			{ s: 'Windows 95', r: /(Windows 95|Win95|Windows_95)/ },
			{ s: 'Windows NT 4.0', r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/ },
			{ s: 'Windows CE', r: /Windows CE/ },
			{ s: 'Windows 3.11', r: /Win16/ },
			{ s: DeviceOS.WINDOWS_MOBILE, r: /Windows Phone/ },
			{ s: DeviceOS.ANDROID, r: /Android/ },
			{ s: DeviceOS.OPEN_BSD, r: /OpenBSD/ },
			{ s: DeviceOS.SUN_OS, r: /SunOS/ },
			{ s: DeviceOS.LINUX, r: /(Linux|X11)/ },
			{ s: DeviceOS.IOS, r: /(iPhone|iPad|iPod)/ },
			{ s: DeviceOS.MAC_OSX, r: /Mac OS X/ },
			{ s: DeviceOS.MAC_OS, r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/ },
			{ s: DeviceOS.QNX, r: /QNX/ },
			{ s: DeviceOS.UNIX, r: /UNIX/ },
			{ s: DeviceOS.BeOS, r: /BeOS/ },
			{ s: DeviceOS.OS2, r: /OS\/2/ },
			{ s: DeviceOS.SEARCH_BOT, r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/ }
		];
		for (let id in clientStrings) {
			let cs = clientStrings[id];
			if (cs.r.test(nAgent)) {
				osName = cs.s;
				break;
			}
		}

		if (osName != DeviceOS.WINDOWS_MOBILE && /Windows/.test(osName)) {
			osVersion = /Windows (.*)/.exec(osName)[1];
			osName = DeviceOS.WINDOWS;
		}

		switch (osName) {
			case DeviceOS.MAC_OSX:
				osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgent)[1];
				break;

			case DeviceOS.ANDROID:
				osVersion = /Android ([\.\_\d]+)/.exec(nAgent)[1];
				break;

			case DeviceOS.IOS:
				osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVersion);
				osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
				break;
		}

		this.data.osName = osName;
		this.data.osVersion = osVersion;
	}

    /**
     * Detect Browser
     * @private
     */
	private _detectBrowser(): void {
		let browserVersion: number;
		let browserName: string = navigator.appName;
		let userAgent: string = navigator.userAgent;
		let fullVersion: string = '' + parseFloat(navigator.appVersion);
		let nameOffset, verOffset, ix;

		// In Opera, the true version is after 'Opera' or after 'Version'
		if ((verOffset = userAgent.indexOf('Opera')) != -1) {
			browserName = BrowserName.OPERA;
			fullVersion = userAgent.substring(verOffset + 6);
			if ((verOffset = userAgent.indexOf('Version')) != -1) fullVersion = userAgent.substring(verOffset + 8);
		}

		// In MSIE, the true version is after 'MSIE' in userAgent
		else if (this.isIE()) {
			browserName = BrowserName.INTERNET_EXPLORER;
			fullVersion = ''+this.getInternetExplorerVersion();
		}

		//Microsoft Edge
			else if ((verOffset = userAgent.indexOf('Edge')) != -1){
			browserName = BrowserName.EDGE;
			fullVersion = userAgent.substring(verOffset + 5);
		}

		// In Chrome, the true version is after 'Chrome'
		else if ((verOffset = userAgent.indexOf('Chrome')) != -1) {
			browserName = BrowserName.CHROME;
			fullVersion = userAgent.substring(verOffset + 7);
		}

		// In Safari, the true version is after 'Safari' or after 'Version'
		else if ((verOffset = userAgent.indexOf('Safari')) != -1) {
			browserName = BrowserName.SAFARI;
			fullVersion = userAgent.substring(verOffset + 7);
			if ((verOffset = userAgent.indexOf('Version')) != -1) fullVersion = userAgent.substring(verOffset + 8);
		}

		// In Firefox, the true version is after 'Firefox'
		else if ((verOffset = userAgent.indexOf('Firefox')) != -1) {
			browserName = BrowserName.FIREFOX;
			fullVersion = userAgent.substring(verOffset + 8);
		}

		// In most other browsers, 'name/version' is at the end of userAgent
		else if ((nameOffset = userAgent.lastIndexOf(' ') + 1) < (verOffset = userAgent.lastIndexOf('/'))) {
			browserName = userAgent.substring(nameOffset, verOffset);
			fullVersion = userAgent.substring(verOffset + 1);
			if (browserName.toLowerCase() == browserName.toUpperCase()) {
				browserName = navigator.appName;
			}
		}

		// trim the fullVersion string at semicolon/space if present
		if ((ix = fullVersion.indexOf(';')) != -1) fullVersion = fullVersion.substring(0, ix);
		if ((ix = fullVersion.indexOf(' ')) != -1) fullVersion = fullVersion.substring(0, ix);

		browserVersion = parseInt('' + fullVersion, 10);
		if (isNaN(browserVersion)) {
			fullVersion = '' + parseFloat(navigator.appVersion);
			browserVersion = parseInt(navigator.appVersion, 10);
		}

		this.data.browserName = browserName;
		this.data.browserVersion = browserVersion;
	}

	private isIE():boolean {
		return ((navigator.appName == 'Microsoft Internet Explorer') || ((navigator.appName == 'Netscape') && (new RegExp('Trident/.*rv:([0-9]{1,}[\.0-9]{0,})').exec(navigator.userAgent) != null)));
	}

	private getInternetExplorerVersion():number
{
	let rv = -1;
	if (navigator.appName == 'Microsoft Internet Explorer')
	{
		let ua = navigator.userAgent;
		let re  = new RegExp('MSIE ([0-9]{1,}[\.0-9]{0,})');
		if (re.exec(ua) != null)
			rv = parseFloat( RegExp.$1 );
	}
	else if (navigator.appName == 'Netscape')
	{
		let ua = navigator.userAgent;
		let re  = new RegExp('Trident/.*rv:([0-9]{1,}[\.0-9]{0,})');
		if (re.exec(ua) != null)
			rv = parseFloat( RegExp.$1 );
	}
	return rv;
}

    /**
     * Detect what the aspect ratio is in landscape mode
     * @private
     */
	private _detectAspectRatio(): void {
		let width: number = Math.max(window.screen.availWidth, window.screen.availHeight);
		let height: number = Math.min(window.screen.availWidth, window.screen.availHeight);
		this.data.aspectRatio = width / height;
	}
}
