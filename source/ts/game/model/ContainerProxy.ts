const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');
import Container = PIXI.Container;

/**
 * The purpose of this class is two fold:
 *  - Allow any mediator to access the containers without the need to pass around
 *  - To encourage developers to reuse containers across states, rather than destroy/create continuously
 */
export class ContainerProxy extends mvc.Proxy<{[key:string]:Container}> {

	public static NAME: string = 'ContainerProxy';

	constructor(containerDictionary:{[key:string]:Container}) {
		super(ContainerProxy.NAME, containerDictionary);
	}

	public getContainer(containerName:string):Container{
		if(!this.data[containerName]) {
			console.warn('Non existent container requested, creating new container with container name :' + containerName);
			this.data[containerName] = new Container();
			this.data[containerName].name = containerName;
		}
		return this.data[containerName];
	}
}
