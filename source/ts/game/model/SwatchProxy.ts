import {GameNotification} from '../notifications/GameNotification';
import {SwatchesVO} from './vo/SwatchesVO';

const mvc:typeof puremvc = require('puremvc-typescript-standard-framework');

export class SwatchProxy extends mvc.Proxy<SwatchesVO> {

    public static NAME: string = 'SwatchProxy';

    constructor() {
        super(SwatchProxy.NAME, new SwatchesVO());
    }

    public get numSwatches():number {
        return this.data.numSwatches;
    }

    public set numSwatches(num:number) {
        this.data.numSwatches = num;
        this.sendNotification(GameNotification.NUM_SWATCHES_UPDATED, this.data.numSwatches);
    }

    public get swatchColors():number[] {
        return this.data.colors;
    }

    public set swatchColors(cols:number[]) {
        this.data.colors = cols;
        this.sendNotification(GameNotification.SWATCH_COLORS_UPDATED, this.data.colors);
    }
}