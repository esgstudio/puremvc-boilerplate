/**
 * Device OS
 * a list of OS names
 * @enum
 */
export class DeviceOS {

	public static ANDROID: string = 'Android';
	public static IOS: string = 'iOS';
	public static CHROME_OS: string = 'Chrome OS';
	public static OPEN_BSD: string = 'Open BSD';
	public static SUN_OS: string = 'Sun OS';
	public static LINUX: string = 'Linux';
	public static UNIX: string = 'UNIX';
	public static QNX: string = 'QNX';
	public static BeOS: string = 'BeOS';
	public static OS2: string = 'OS/2';
	public static SEARCH_BOT: string = 'Search Bot';
	public static MAC_OS: string = 'Mac OS';
	public static MAC_OSX: string = 'Mac OSX';
	public static WINDOWS: string = 'Windows';
	public static WINDOWS_MOBILE: string = 'Windows Mobile';


}
