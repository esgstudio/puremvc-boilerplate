/**
 * Browser Name
 * list of browser names
 * @enum
 */
export class BrowserName {

	public static FIREFOX: string = 'Firefox';
	public static INTERNET_EXPLORER: string = 'Internet Explorer';
	public static CHROME: string = 'Chrome';
	public static SAFARI: string = 'Safari';
	public static OPERA: string = 'Opera';
	public static EDGE: string = 'Edge';
	public static OTHER: string = 'Other';

}
