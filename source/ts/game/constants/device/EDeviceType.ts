/**
 * Device Type
 * list of device types, all of these are totally legit
 * @enum
 */
export enum EDeviceType {
	MOBILE,
	TABLET,
	DESKTOP
}
