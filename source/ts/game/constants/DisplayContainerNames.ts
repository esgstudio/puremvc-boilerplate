export class DisplayContainerNames{

	private static _namespace:string = 'DisplayContainerNames:';


	public static GAME_CONTAINER:string 		= DisplayContainerNames._namespace + 'GAME_CONTAINER';
	public static BACKGROUND_CONTAINER:string 	= DisplayContainerNames._namespace + 'BACKGROUND_CONTAINER';
	public static FOREGROUND_CONTAINER:string 	= DisplayContainerNames._namespace + 'FOREGROUND_CONTAINER';
}
