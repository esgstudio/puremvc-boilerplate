/**
 * Now - shim
 * @type Function
 * @private
 */

export class DateUtils {

    public static now() : Function{
        var _nowFunction:Function = (window.performance && window.performance.now ?
            window.performance.now.bind(window.performance) : Date.now ?
            Date.now.bind(Date) : function() { return new Date().getTime(); });
        return _nowFunction;
    }
}

