type Signal = signals.Signal;
type SignalBinding = signals.SignalBinding;

interface TypedSignal<T> extends Signal {

  add(listener:(value:T)=>void, listenerContext?: any, priority?: number): SignalBinding;
  
  dispatch(value:T): void;
}

interface TypedSignal2Params<T, U> extends Signal {

  add(listener:(value:T, value2:U)=>void, listenerContext?: any, priority?: number): SignalBinding;
  
  dispatch(value:T, value2:U): void;
}

interface TypedSignal3Params<T, U, V> extends Signal {

  add(listener:(value:T, value2:U, value3:V)=>void, listenerContext?: any, priority?: number): SignalBinding;
  
  dispatch(value:T, value2:U, value3:V): void;
}

interface TypedSignal4Params<T, U, V, W> extends Signal {

  add(listener:(value:T, value2:U, value3:V, value4:W)=>void, listenerContext?: any, priority?: number): SignalBinding;
  
  dispatch(value:T, value2:U, value3:V, value4:W): void;
}