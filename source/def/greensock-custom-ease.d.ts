declare class CustomEase extends Ease {
	public static create(ID:String, data?:String, config?:String ):CustomEase;
	public get (ID:String):CustomEase;
	public getRatio(progress:Number ):number;
	public getSVGData ( ease:any, vars:Object ) : String;
}
