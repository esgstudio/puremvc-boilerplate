declare module 'domReady' {
    function domReady(callback: () => any): void;
    export = domReady;
}