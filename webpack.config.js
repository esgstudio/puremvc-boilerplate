var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: './source/ts/App.ts',
    output: {
        filename: 'game.js'
    },
    resolve: {
        modules: [path.resolve(__dirname, "source/ts"), "node_modules"],
        extensions: ['.ts', '.js'],
        alias: {
            'TweenLite': 'gsap/src/uncompressed/TweenLite',
            'TweenMax': 'gsap/src/uncompressed/TweenMax',
            'puremvc': 'puremvc-typescript-standard-framework',
            'gsap-custom-ease': path.resolve(__dirname, 'source/libs/CustomEase.js')
        }
    },

    module: {
        loaders: [
            { test: /\.ts$/, loader: 'ts-loader' }
        ],
        noParse: [ /.*(pixi-particles\.js).*/ ]
    },
    plugins: [],
    watch: true
};
