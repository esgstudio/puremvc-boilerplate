**PureMVC Boilerplate**

This repo contains a boilerplate project that should instantiate a simple single page app that is driven by the PureMVC framework.

---

## Setup

To launch, follow these instructions.

1. If not installed globally, run npm install gulp.
2. Then on a mac, run bash setup-project.sh
3. OR on Windows run setup-project.bat
4. To compile and launch the app, just run gulp

---

## Additional Notes

TBC.