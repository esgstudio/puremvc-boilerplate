var gulp = require('gulp'); 							// Gulp
var changed = require('gulp-changed');                  // Only affect changed files
var dir = require('./path-info').dir;
var constants = require('./constants');
var browserSync = require('browser-sync');

// FONTS --------------------------------------------------------------------------------------------------------------
gulp.task('copy-fonts', function () {
    return gulp.src(dir.assets.fonts+'/**/*')
        .pipe(changed(dir.build.fonts))
        .pipe(gulp.dest(dir.build.fonts))
        .pipe(browserSync.get(constants.BrowserSync.IDENTIFIER).stream());
});
