
var getPackageJson = require('./getPackageJson');
var gulp = require('gulp'); 							// Gulp
var replace = require('gulp-replace');					// do string replacement on streams
var dir = require('./path-info').dir;

// VERSION ------------------------------------------------------------------------------------------------------------
gulp.task('version', function () {
    var package = getPackageJson();
    return gulp.src([dir.source.ts + '/game/constants/GameDetails.ts'])
        .pipe(replace(/\d+\.\d+\.\d+/g, package.version))
        .pipe(gulp.dest(dir.source.ts + '/game/constants/'))
});
