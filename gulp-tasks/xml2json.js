var gulp = require('gulp'); 							// Gulp
var rename = require('gulp-rename');
var xml2json = require('gulp-xml2json');

var dir = require('./path-info').dir;

gulp.task('xml2json', function() {
	return gulp.src(dir.assets.lang + '/*.xml')
		.pipe(xml2json({
			mergeAttrs: true,
			explicitArray: false,
			explicitRoot: false
		}))
		.pipe(rename({extname: '.json'}))
		.pipe(gulp.dest(dir.source.lang));
});