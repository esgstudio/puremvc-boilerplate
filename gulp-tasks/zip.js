var gulp = require('gulp'); 							// Gulp
var zip = require('gulp-zip');							// For concatenating hashes into one file

var dir = require('./path-info').dir;
var getPackageJson = require('./getPackageJson');

gulp.task('zip', function(){
    var package = getPackageJson();
    var path = dir.build.root;
	return gulp.src(path + "**/**", {base: path})
        .pipe(zip(package.name + "_v" + package.version + ".zip"))
        .pipe(gulp.dest(dir.build.root + "/../"));
})