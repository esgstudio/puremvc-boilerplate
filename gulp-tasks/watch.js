var gulp = require('gulp'); 							// Gulp
var watch = require('gulp-watch');                      // Watch

var dir = require('./path-info').dir;

var constants = require('./constants');
var cc = constants.ConsoleColors;

gulp.task('watch', [], function() {
	gulp.watch(dir.source.json+'/**/*', ['copy-json']).on('change', reportChange);
	gulp.watch(dir.source.fonts+'/**/*', ['copy-fonts']).on('change', reportChange);
	gulp.watch(dir.source.css+'/**/*', ['copy-css']).on('change', reportChange);
	gulp.watch(dir.source.html+'/**/*', ['html']).on('change', reportChange);
	gulp.watch(dir.source.images+'/**/*', ['copy-images']).on('change', reportChange);
});

/**
 * Report Change
 * @param {{path:String, type:String}} event
 */
function reportChange(event){
    console.log('File ' + cc.pink + event.path + cc.clear + ' was ' + cc.cyan + event.type + cc.clear + ' running tasks...');
};
