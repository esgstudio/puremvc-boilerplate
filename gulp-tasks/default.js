var gulp = require('gulp'); 							// Gulp
var runSequence = require('run-sequence');              // Manage async tasks
var pathInfo = require('./path-info');
var setDirectory = pathInfo.setDirectory;

/**
 * @default
 * this is the default task that's run when you run gulp
 */

gulp.task('default', function () {

	var tasks = [
        ['clean'],
		['ts'],
		['html', 'copy-css', 'copy-fonts'],
        ['watch', 'browser-sync']
	];

	setDirectory('build/app');
	runSequence.apply(this, tasks);
});

