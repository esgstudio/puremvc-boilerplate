var fs = require('fs');

//this is necessary to stop the contents of package.json being cached between tasks
var getPackageJson = function () {
  return JSON.parse(fs.readFileSync('./package.json', 'utf8'));
};

module.exports = getPackageJson;