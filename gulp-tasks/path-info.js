var dir = {source: {}, library: {}, build: {}, assets: {}, binary: {}};

function setDirectory(buildRoot) {

	// SOURCE -------------------------------------------------
	dir.source.root 	= './source';
	dir.source.libs 	= dir.source.root + '/libs';
	dir.source.json 	= dir.source.root + '/json';
	dir.source.ts 		= dir.source.root + '/ts';
	dir.source.html 	= dir.source.root + '/html';
	dir.source.css 		= dir.source.root + '/css';
	dir.source.lang 	= dir.source.root + '/json/lang/';

	// LIBRARY ------------------------------------------------
	dir.library.bower 	= 'bower_components';
	dir.library.npm 	= 'node_modules';

	// ASSETS -------------------------------------------------
	dir.assets.root 	= 'assets';
	dir.assets.images 	= {};
	dir.assets.images.source 	= ''; // Set by the resize task
	dir.assets.images.root 		= dir.assets.root + '/images/';
	dir.assets.images.high 		= dir.assets.images.root + '/high/';
	dir.assets.images.mid 		= dir.assets.images.root + '/mid/';
	dir.assets.images.low 		= dir.assets.images.root + '/low/';

	dir.assets.fonts 		= dir.assets.root + '/fonts/';
	dir.assets.sounds		= {};
	dir.assets.sounds.sfx 	= dir.assets.root + '/sounds/sfx/';
	dir.assets.sounds.mus 	= dir.assets.root + '/sounds/mus/';
	dir.assets.lang 		= dir.assets.root + '/lang/';

	// BUILD --------------------------------------------------
	dir.build.root 			= buildRoot;
	dir.build.js			= dir.build.root + '/js';
	dir.build.css 			= dir.build.root + '/css/';
	dir.build.json 			= dir.build.root + '/json/';
	dir.build.images 		= dir.build.root + '/images';
	dir.build.fonts 		= dir.build.root + '/fonts/';
	dir.build.sounds		= {};
	dir.build.sounds.sfx 	= dir.build.root + '/sounds/sfx/';
	dir.build.sounds.mus 	= dir.build.root + '/sounds/mus/';
	dir.build.html 			= dir.build.root;
}

module.exports = {
    dir: dir,
    setDirectory: setDirectory
}