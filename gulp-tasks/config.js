var constants = require('./constants');
var Environments = constants.Environments;

var buildConfig = {
	bumpIncremement:null,
	environment:Environments.STANDALONE,
	tsClass: "StandaloneApp",
	jsFiles: ["js/game.js"]
};

module.exports = buildConfig;