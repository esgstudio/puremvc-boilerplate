var gulp = require('gulp'); 
var webpack = require('webpack');								// Gulp
var webpackStream = require('webpack-stream');				// compile and concat typescript and libs
var sourcemaps = require('gulp-sourcemaps');            // Source Maps
var dir = require('./path-info').dir;
var buildConfig = require("./config");
var constants = require('./constants');
var Environments = constants.Environments;
var browserSync = require('browser-sync');

// TYPESCRIPT ---------------------------------------------------------------------------------------------------------
gulp.task('ts', function() {
	var isProduction = buildConfig.environment == Environments.PRODUCTION;

	var src = dir.source.ts + '/' + buildConfig.tsClass + '.ts';
	
	var config = require('../webpack.config.js');
	config.entry = src;
	if (isProduction)
	{
		//minify JS if in production
		config.plugins.push(new webpack.optimize.UglifyJsPlugin({
			compress: {
				//remove console.logs
				pure_funcs: [ 'console.log' ],
				warnings: false
			}
		}));			
	} else {
		//otherwise, enable source maps
		config.devtool = 'source-map';
	}


	var stream = gulp.src(src)
	.pipe(webpackStream(config));

	if (!isProduction)
	{
		//write sourcemap
		stream = stream.pipe(sourcemaps.write('./'))
	}

	stream = stream.pipe(gulp.dest(dir.build.js))
	.pipe(browserSync.get(constants.BrowserSync.IDENTIFIER).stream());
});