const gulp = require('gulp');
const dir = require('./path-info').dir;
const constants = require('./constants');
const browserSync = require('browser-sync').create(constants.BrowserSync.IDENTIFIER); // Open a new browser session and load changes instantly

gulp.task('browser-sync', function() {

    var standaloneSyncConfig = {
        port: 8098,
        server: {
            baseDir: [dir.build.root],
            index: 'index.html'
        }
    };

    browserSync.init(standaloneSyncConfig);
});
