var gulp = require('gulp'); 							// Gulp
var changed = require('gulp-changed');                  // Only affect changed files
var dir = require('./path-info').dir;
var constants = require('./constants');
var browserSync = require('browser-sync');

gulp.task('copy-css', function () {
    return gulp.src([dir.source.css+'/**/*'])
        .pipe(changed(dir.build.css))
        .pipe(gulp.dest(dir.build.css))
        .pipe(browserSync.get(constants.BrowserSync.IDENTIFIER).stream());
});
