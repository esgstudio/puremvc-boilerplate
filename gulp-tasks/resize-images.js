var gulp = require('gulp'); 							// Gulp
var argv = require('yargs').argv;						// Yargs the modern, pirate-themed, successor to optimist.
var imageSize = require('image-size');					// Get dimensions of any image file
var foreach = require('gulp-foreach');					// Send each file in a stream down its own stream
var imageResize = require('gulp-image-resize');			// Resizing images made easy.
var imageMin = require('gulp-imagemin');				// Minify PNG, JPEG, GIF and SVG images
var runSequence = require('run-sequence');              // Manage async tasks
var jsonEditor = require("gulp-json-editor");			// A gulp plugin to edit JSON object
var xmlEditor = require("gulp-edit-xml");				// A gulp plugin to edit XML document (libxmljs wrapper)

var cc = require('./constants').ConsoleColors;

var pathInfo = require('./path-info');
var setDirectory = pathInfo.setDirectory;
var dir = pathInfo.dir;

// RESIZE ASSETS ------------------------------------------
gulp.task('resize', function () {
	showResizeTitle();
	setDirectory('build/app', 'en');
	/** @type {String} */
	var source = argv.source;
	if (source)
	{
		dir.assets.images.source = source;
		runSequence(
			'resize-json',
			'resize-xml',
			'resize-images'
		);
	}
	else
	{
		console.log(cc.pink+'ERROR:'+cc.clear+' missing source parameter');
		console.log('include the source param like this:');
		console.log(cc.yellow+'gulp resize --source folder/path/to/image/source/'+cc.clear);
		console.log('make sure the file path ends in a /');
	}
});

/**
 * Show Resize Title
 */
function showResizeTitle() {
	console.log(cc.cyan+"     _____           _");
	console.log("    |  __ \\         (_)");
	console.log("    | |__) |___  ___ _ _______");
	console.log("    |  _  // _ \\/ __| |_  / _ \\");
	console.log("    | | \\ \\  __/\\__ \\ |/ /  __/");
	console.log("    |_|  \\_\\___||___/_/___\\___|"+cc.clear);
	console.log(cc.cyan+"----------------------------------------------------------------------"+cc.clear);
	console.log(cc.pink+'High: '+cc.clear+resToString(res.high)+' - '+cc.cyan+'the source assets need to built for this resolution');
	console.log(cc.pink+'Mid: '+cc.clear+resToString(res.mid));
	console.log(cc.pink+'Low: '+cc.clear+resToString(res.low));
	console.log(cc.pink+'Source:', argv.source == undefined ? cc.cyan+'Missing source parameter!' : cc.clear+argv.source);
	console.log(cc.cyan+"----------------------------------------------------------------------"+cc.clear);
};

// RESIZE IMAGES ------------------------------------------------------------------------------------------------------
/**
 * Resolutions
 * @type {{high: {width: number, height: number}, mid: {width: number, height: number}, low: {width: number, height: number}}}
 */
var res =
{
	high:{width:1920,height:1080},
	mid:{width:1440,height:810},
	low:{width:960,height:540}
};

/**
 * Scales
 * @type {{large: number, medium: number, small: number}}
 */
var scales = {
	high:1,
	mid:(1/res.high.width)*res.mid.width,
	low:(1/res.high.width)*res.low.width
};

/**
 * Get Image Size
 * @param {String} imageURL
 * @param {Number} scale
 * @return {{width:Number,height:Number}}
 */
function getImageSize(imageURL,scale) {
	/** @type {{width:Number, height:Number, type:String }} */
	var dimentions = imageSize(imageURL);
	var newDimentions = {width:Math.floor(parseInt(dimentions.width)*scale),height:Math.floor(parseInt(dimentions.height)*scale)};
	console.log('width '+cc.pink+dimentions.width+cc.clear+" to "+cc.yellow+newDimentions.width+cc.clear);
	return newDimentions;
}

gulp.task('resize-images', function() {
	console.log(cc.cyan+"-------------------------- "+cc.yellow+"Resize Images"+cc.cyan+" --------------------------"+cc.clear);
	return gulp.src([dir.assets.images.source+'**/*.{jpg,png}'])
		.pipe(foreach(function(stream, file) {
			return stream
				.pipe(imageResize( getImageSize(file.history[0], scales.high) ) )
				.pipe( imageMin({ progressive: true }) )
				.pipe(gulp.dest(dir.assets.images.high))
				.pipe(imageResize( getImageSize(file.history[0], scales.mid) ) )
				.pipe( imageMin({ progressive: true }) )
				.pipe(gulp.dest(dir.assets.images.mid))
				.pipe(imageResize( getImageSize(file.history[0], scales.low) ) )
				.pipe( imageMin({ progressive: true }) )
				.pipe(gulp.dest(dir.assets.images.low))
				.on('end', function(){
					console.log(cc.yellow+'Resized'+cc.cyan,file.history[0].split('/').pop()+cc.clear);
				})
		}))
});

/**
 * Scale Data
 * @param {Object} data
 * @param {Array<String>} variables
 * @param {Number} scale
 * @return {Object}
 */
function scaleData(data,variables,scale) {
	/** @type {String} */
	var key;
	/** @type {Number} */
	var v = 0;
	/** @type {String} */
	var variable = '';

	for (key in data)
	{
		v = variables.length;
		while (v-- > 0) {
			variable = variables[v];
			if (key == variable){
				data[key] = Math.floor(parseInt(data[key]) * scale);
			}
		}

		if (typeof data[key] == 'object')
		{
			data[key] = scaleData(data[key],variables,scale);
		}
	}
	return data;
};

//       _  _____  ____  _   _   --------------------------------------------------------------------------------------
//      | |/ ____|/ __ \| \ | |
//      | | (___ | |  | |  \| |
//  _   | |\___ \| |  | | . ` |
// | |__| |____) | |__| | |\  |
//  \____/|_____/ \____/|_| \_|  --------------------------------------------------------------------------------------

gulp.task('resize-json', function(){
	console.log(cc.cyan+"--------------------------- "+cc.yellow+"Resize JSON"+cc.cyan+" ---------------------------"+cc.clear);
	return runSequence('resize-json-high','resize-json-mid','resize-json-low');
});

gulp.task('resize-json-high', function(){ return gulp.src([dir.assets.images.source+'**/*.json']).pipe(_resizeJson(scales.high, dir.assets.images.high)) });
gulp.task('resize-json-mid', function(){ return gulp.src([dir.assets.images.source+'**/*.json']).pipe(_resizeJson(scales.mid, dir.assets.images.mid)) });
gulp.task('resize-json-low', function(){ return gulp.src([dir.assets.images.source+'**/*.json']).pipe(_resizeJson(scales.low, dir.assets.images.low)) });

/**
 * Resize JSON
 * @param {Number} scale - the scaling factor from the orignial/high resolution
 * @param {String} dest - the destination folder
 * @private
 */
function _resizeJson(scale,dest){
	return foreach(function(stream, file) {
		return stream
			.pipe(jsonEditor(function(json){
				json = scaleData(json,['x','y','h','w'],scale);
				if (json.meta) { json.meta.scale = scale; }
				return json;
			}))
			.pipe(gulp.dest(dest))
			.on('end', function(){
				console.log(cc.yellow+'Resized'+cc.cyan,file.history[0].split('/').pop());
			})
	});
}


//   __   ____  __ _        -------------------------------------------------------------------------------------------
//   \ \ / /  \/  | |
//    \ V /| \  / | |
//     > < | |\/| | |
//    / . \| |  | | |____
//   /_/ \_\_|  |_|______|  -------------------------------------------------------------------------------------------

gulp.task('resize-xml', function(){
	console.log(cc.cyan+"--------------------------- "+cc.yellow+"Resize XML"+cc.cyan+" ---------------------------"+cc.clear);
	return runSequence('resize-xml-high','resize-xml-mid','resize-xml-low');
});

gulp.task('resize-xml-high', function(){ return gulp.src([dir.assets.images.source+'**/*.xml']).pipe(_resizeXML(scales.high, dir.assets.images.high)) });
gulp.task('resize-xml-mid', function(){ return gulp.src([dir.assets.images.source+'**/*.xml']).pipe(_resizeXML(scales.mid, dir.assets.images.mid)) });
gulp.task('resize-xml-low', function(){ return gulp.src([dir.assets.images.source+'**/*.xml']).pipe(_resizeXML(scales.low, dir.assets.images.low)) });

/**
 * Resize XML
 * @param {Number} scale - the scaling factor from the orignial/high resolution
 * @param {String} dest - the destination folder
 * @private
 */
function _resizeXML(scale,dest) {
	return foreach(function(stream, file) {
		return stream
			.pipe(xmlEditor(function(xml) {
				return scaleData(xml,['width','height','x','y'],scale);
			}))
			.pipe(gulp.dest(dest))
			.on('end', function(){
				console.log(cc.yellow+'Resized'+cc.cyan,file.history[0].split('/').pop());
			})
	});
}

/**
 * Resolution to String
 * @param {width:Number,height:Number} res
 * @return {String}
 */
function resToString(res)
{
	return res.width + 'x' + res.height
}
