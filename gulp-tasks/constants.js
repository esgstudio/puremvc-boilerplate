// --------------------------------------------------------------------------------------------------------------------
// Constants
// --------------------------------------------------------------------------------------------------------------------
var Environments = {
	DEV:"DEV",
	PRODUCTION:"PRODUCTION"
};

var Platforms = {
	STANDALONE:"STANDALONE"
};

var Bitrates = {
	SFX: "128k",
	MUSIC: "96k"
};

var ConsoleColors = {
	cyan:'\033[0;36m',
	yellow:'\033[0;33m',
	pink:'\033[0;35m',
	clear:'\033[0m'
};

var BrowserSync = {
    IDENTIFIER: "Ashs Mum"
};

module.exports = {
    Environments: Environments,
    Platforms: Platforms,
    Bitrates: Bitrates,
    BrowserSync: BrowserSync,
    ConsoleColors: ConsoleColors
};