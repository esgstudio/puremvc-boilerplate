var gulp = require('gulp'); 							// Gulp
var dir = require('./path-info').dir;
var constants = require('./constants');
var browserSync = require('browser-sync');

// IMAGES -------------------------------------------------------------------------------------------------------------
gulp.task('copy-images', function () {
    return gulp.src([dir.assets.images.root+'/**/*'])
        .pipe(gulp.dest(dir.build.images))
        .pipe(browserSync.get(constants.BrowserSync.IDENTIFIER).stream());
});
