var gulp = require('gulp'); 							// Gulp
var minifyCSS = require('gulp-minify-css');
var minifyJSON = require('gulp-jsonminify');

var dir = require('./path-info').dir;

gulp.task('minify', ['minify-css', 'minify-json'], function (complete) {
    complete();
});

gulp.task('minify-css', function () {
    var root = dir.build.root;
	return gulp.src(root + '/**/*.css')
        .pipe(minifyCSS())
		.pipe(gulp.dest(root))
});

gulp.task('minify-json', function () {
    var root = dir.build.root;
	return gulp.src(root + '/**/*.json')
        .pipe(minifyJSON())
		.pipe(gulp.dest(root))
});