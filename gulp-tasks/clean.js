var gulp = require('gulp'); 							// Gulp
var del = require('del');                               // Delete files

var dir = require('./path-info').dir;

gulp.task('clean', function() {
	return del(dir.build.root, { force: true });
});