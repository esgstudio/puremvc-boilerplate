var gulp = require('gulp'); 							// Gulp
var changed = require('gulp-changed');                  // Only affect changed files
var dir = require('./path-info').dir;
var constants = require('./constants');
var browserSync = require('browser-sync');

gulp.task('copy-json', function () {
    return gulp.src([dir.source.json+'/**/*'])
        .pipe(changed(dir.build.json))
        .pipe(gulp.dest(dir.build.json))
        .pipe(browserSync.get(constants.BrowserSync.IDENTIFIER).stream());
});


