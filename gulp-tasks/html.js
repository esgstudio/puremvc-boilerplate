var gulp = require('gulp'); 							// Gulp
var changed = require('gulp-changed');                  // Only affect changed files
var htmlreplace = require('gulp-html-replace');			// replace parts of the html
var dir = require('./path-info').dir;
var buildConfig = require("./config");
var constants = require('./constants');
var browserSync = require('browser-sync');

gulp.task('html', function () {
    return gulp.src([dir.source.html+'/**/*'])
        .pipe(changed(dir.build.html))
        .pipe(htmlreplace({
            js: buildConfig.jsFiles
        }))
        .pipe(gulp.dest(dir.build.html))
        .pipe(browserSync.get(constants.BrowserSync.IDENTIFIER).stream());
});
