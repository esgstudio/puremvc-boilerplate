var gulp = require('gulp'); 							// Gulp
var argv = require('yargs').argv;						// Yargs the modern, pirate-themed, successor to optimist.
var audiosprite = require('gulp-audiosprite');			// Audiosprite; Pray to god that Keith installed this correctly
var ffmpeg = require('gulp-fluent-ffmpeg');				// Fluent; Sound Conversion for Music
var runSequence = require('run-sequence');              // Manage async tasks
var jsonEditor = require("gulp-json-editor");			// A gulp plugin to edit JSON object

var pathInfo = require('./path-info');
var setDirectory = pathInfo.setDirectory;
var dir = pathInfo.dir;

var constants = require('./constants');
var cc = constants.ConsoleColors;



// SOUND PACKING ---------------------------------------------
//NOTE: --source should now always be assets/sounds/src/
gulp.task('convertaudio', function(){
	var source = argv.source;
	if(source){
		setDirectory('', '');
		runSequence('convertSFX', 'fixSFXJson', 'convertMusic')
	} else {
		console.log(cc.pink+'ERROR:'+cc.clear+' missing source parameter');
		console.log('include the source param like this:');
		console.log(cc.yellow+'gulp convertaudio --source folder/path/to/image/source/'+cc.clear);
		console.log('make sure the file path ends in a /');
	}
});

gulp.task('convertSFX', function(){
	var source = argv.source;
	console.log('-Packing SFX from '+source+"sfx/");
	console.log('-Building to '+dir.assets.sounds.sfx);
	return gulp.src(source+"sfx/*.*")
		.pipe(audiosprite({
			output: 'sfx',		// Outputs sfx.mp3, sfx.ogg, sfx.m4a and sfx.ac3
			format:"howler",	// Howler Format

			channels: 2,		// Stereo
			silence: 0.5,		// Adds Silence
			gap:0.1,			// Audio Gap of 0.1 second
			minlength: 0.5		// Minimum Duration of half a second
		}))
		.pipe(gulp.dest(dir.assets.sounds.sfx));
})

gulp.task('fixSFXJson', function(){
	var path = dir.assets.sounds.sfx;
	return gulp.src(path + "/sfx.json")
		.pipe(jsonEditor(function(json){
			json.src = json.urls;
			delete json.urls;
			return json;
		}))
		.pipe(gulp.dest(path));
});

gulp.task('convertMusic', function(){
	var source = argv.source;
	console.log('-Converting MUSIC from '+source+"mus/");
	console.log('-Building to '+dir.assets.sounds.mus);
	return runSequence('convertMusicMP3', 'convertMusicOGG', 'convertMusicAC3')
})

gulp.task('convertMusicMP3', function(){
	var source = argv.source;
	return gulp.src(source+"mus/*.*")
		.pipe(ffmpeg('mp3', function(cmd){
		return cmd
			.audioBitrate(constants.Bitrates.MUSIC)
			.audioChannels(2)
			.audioCodec('libmp3lame')
	}))
		.pipe(gulp.dest(dir.assets.sounds.mus));
})

gulp.task('convertMusicOGG', function(){
	var source = argv.source;
	return gulp.src(source+"mus/*.*")
		.pipe(ffmpeg('ogg', function(cmd){
			return cmd
				.audioBitrate(constants.Bitrates.MUSIC)
				.audioChannels(2)
		}))
		.pipe(gulp.dest(dir.assets.sounds.mus));
})


gulp.task('convertMusicAC3', function(){
	var source = argv.source;
	return gulp.src(source+"mus/*.*")
		.pipe(ffmpeg('ac3', function(cmd){
			return cmd
				.audioBitrate(constants.Bitrates.MUSIC)
				.audioChannels(2)
		}))
		.pipe(gulp.dest(dir.assets.sounds.mus));
})