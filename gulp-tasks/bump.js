var gulp = require('gulp'); 							// Gulp
var bump = require('gulp-bump');						// Bump any Semver version in any file with gulp

var buildConfig = require("./config");

gulp.task('bump', function() {
	return gulp.src('package.json', {base: "../"})
		.pipe(bump({key: "version", type:buildConfig.bumpIncremement}))
		.pipe(gulp.dest('../'));
});