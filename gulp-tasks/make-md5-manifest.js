var gulp = require('gulp'); 
var dir = require('./path-info').dir;
var hash = require('gulp-hash-manifest');				// For generating a md5 hashes
var concat = require('gulp-concat');					// For concatenating hashes into one file

gulp.task('make-md5-manifest', function() {
	var path = dir.build.root;
	var absPath = __dirname + path;
	var prefixLen = absPath.length + 2;
	return gulp.src(path + "**/**", {base: path})
        .pipe(hash({
			format: function(_path, _hex) {
				return _hex + " " + _path.substr(prefixLen);
			}
		}))
        .pipe(concat('checksum.md5'))
        .pipe(gulp.dest(path ));
});